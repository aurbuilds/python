# Python

## Package list:

- python-openshift: Python client for the OpenShift API

## Usage
Add the following lines to `/etc/pacman.conf`:

    [aurbuilds-python]
    SigLevel = Never
    Server = https://aurbuilds.gitlab.io/python/$arch
